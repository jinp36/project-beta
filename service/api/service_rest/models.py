from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, default=None)
    sold = models.BooleanField(default=False)

    class Meta:
        ordering = ["id"]

    def __str__(self):
        return self.vin


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=10, unique=True)

    class Meta:
        ordering = ["employee_id"]

    def get_api_url(self):
        return reverse("api_technicians", kwargs={"pk": self.id})

    def __str__(self):
        return self.first_name + " " + self.last_name


class Appointment(models.Model):
    date = models.DateTimeField()
    reason = models.TextField(max_length=10000)
    customer = models.CharField(max_length=200)
    vin = models.CharField(max_length=17)

    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    status = models.CharField(max_length=10)

    sold = models.BooleanField(default=False)

    def finished(self):
        self.status = "Finished"
        self.save()

    def cancelled(self):
        self.status = "Cancelled"
        self.save()

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"pk": self.pk})

    def __str__(self):
        return self.customer
