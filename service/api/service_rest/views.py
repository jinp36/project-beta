from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import TechnicianEncoder, AppointmentEncoder
from .models import Technician, Appointment, AutomobileVO
from django.shortcuts import get_object_or_404


@require_http_methods(["GET", "POST"])
def api_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
            )
    else:
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
                )
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Could not create the technician"},
                status=400
            )


@require_http_methods(["DELETE"])
def api_technicians(request, id):
    if request.method == "DELETE":
        technicians = get_object_or_404(Technician, id=id)
        technicians.delete()
        return JsonResponse(
            {"message": "Technician deleted"}
            )


@require_http_methods(["GET", "POST"])
def api_appointment(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder)
    else:
        try:
            content = json.loads(request.body)
            technician_id = content["technician"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
            content["status"] = "Created"
            inventory = AutomobileVO.objects.filter(vin=content["vin"])
            inventory = list(inventory)
            if inventory == []:
                content["sold"] = False
            else:
                content["sold"] = True
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
                )
        except Appointment.DoesNotExist:
            return JsonResponse(
                {"message": "Could not create the appointment"},
                status=400
            )


@require_http_methods(["DELETE"])
def api_appointments(request, id):
    if request.method == "DELETE":
        appointments = get_object_or_404(Appointment, id=id)
        appointments.delete()
        return JsonResponse(
            {"message": "Appointment Deleted"}
            )


@require_http_methods(["PUT"])
def api_finished_appointments(request, id):
    appointment = get_object_or_404(Appointment, id=id)
    appointment.finished()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def api_cancelled_appointments(request, id):
    appointment = get_object_or_404(Appointment, id=id)
    appointment.cancelled()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )
