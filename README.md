# CarCar

Team:

* Mark - Service
* Jin - Sales

## Design
The overall design on the project is to create an application for an automobile dealership. This will allow the dealership to keep track of what automobiles they have in inventory. The application will also allow the dealership to keep track of sales data and service appointments. The application uses REACT frontend and Django backend and is a single page web application.

## Service microservice
I had 3 models which modelled the data for a customer's appointment and information about which technician will be handling the appointment. I also had a value object which got the VIN information from automobiles that were once in inventory. I would use this data to check if a customer who is setting an appointment purchased the vehicle from the dealership by checking whether the VIN entered by the customer matches the one that is stored in the database. Futhermore, when an appointment has been created, an employee can go into the application and update the status of the appointment from cancelled to finished. A service history is also included which will allow for employees to search through completed appointments by the vehicle's VIN number.

## Sales microservice

My models are: Salesperson, Customer, Sales, and AutomobileVO.  I got these names from test_models.py.  The field names were obtained from Learn. In Sales model, originally I thought vin should be a one-to-one field since there is only one vin per transaction, but if I do that I would get error 500 when trying to use the VIN twice.  So, I changed it to foreign key.  I believe trying to incorporate one-to-one relationship for vin is beyond the scope of the current project. Salesperson and customer are foreign keys also.  Also, I used a bunch of field names with get_extra_data from extra encoders inside sales encoder.  These extra fields were used in SalesLIst and SalesHistory.

The sales microservice talks to inventory microservice through the poller.  Once we set manufacturer, model, and automobile data through the frontend by directly accessing inventory microservice api's, vins that are created in inventory are pulled to AutomobileVO model/encoder through the poller.  Then, using the vin information, customer, salesperson and price data, we are able to create sales records.
