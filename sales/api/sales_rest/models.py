from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    import_vin = models.CharField(max_length=17, unique=True)


class Salesperson(models.Model):
    employee_no = models.CharField(max_length=100, unique=True)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone_no = models.CharField(max_length=17)
    address = models.CharField(max_length=200)


class Sales(models.Model):
    vin = models.ForeignKey(
        AutomobileVO,
        related_name="sales_rec_vin",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales_rec_person",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sales_rec_customer",
        on_delete=models.CASCADE,
    )
    price = models.PositiveIntegerField()

    def get_api_url(self):
        return reverse("salesrec_list", kwargs={"id": self.id})
