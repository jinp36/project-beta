from django.contrib import admin
from .models import AutomobileVO, Sales, Salesperson


@admin.register(AutomobileVO)
class VinVOAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "import_href",
        "import_vin",
    )

@admin.register(Sales)
class SalesRecAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "vin",
        "salesperson",
        "customer",
        "price"
    )

@admin.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    pass   
