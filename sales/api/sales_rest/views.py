from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.shortcuts import get_object_or_404
import json
from .models import (
    Salesperson,
    Customer,
    Sales,
    AutomobileVO
)
from .encoders import (
    SalespersonListEncoder,
    SalespersonDetailEncoder,
    CustomerListEncoder,
    CustomerDetailEncoder,
    SalesEncoder,
    AutomobileVOEncoder
)


@require_http_methods(['GET', 'POST'])
def salesperson_list(request):
    if request.method == 'GET':
        salesperson = Salesperson.objects.all()
        return JsonResponse(
            {"sales_persons": salesperson},
            encoder=SalespersonListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False,
        )


@require_http_methods(['DELETE', 'GET', 'PUT'])
def salesperson_detail(request, id):
    if request.method == 'GET':
        salesperson = get_object_or_404(Salesperson, id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False,
        )
    elif request.method == 'DELETE':
        count, _ = Salesperson.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Salesperson.objects.filter(id=id).update(**content)
        salesperson = get_object_or_404(Salesperson, id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonDetailEncoder,
            safe=False,
        )


@require_http_methods(['GET', 'POST'])
def customer_list(request):
    if request.method == 'GET':
        customer = Customer.objects.all()
        return JsonResponse(
            {"customers": customer},
            encoder=CustomerListEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )


@require_http_methods(['DELETE', 'GET', 'PUT'])
def customer_detail(request, id):
    if request.method == 'GET':
        customer = get_object_or_404(Customer, id=id)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )
    elif request.method == 'DELETE':
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Customer.objects.filter(id=id).update(**content)
        customer = get_object_or_404(Customer, id=id)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )


@require_http_methods(['GET', 'POST'])
def salesrec_list(request):
    if request.method == 'GET':
        salesrec = Sales.objects.all()
        return JsonResponse(
            {"sales_records": salesrec},
            encoder=SalesEncoder
        )
    else:
        content = json.loads(request.body)
        sp_id = content["salesperson"]
        salesperson = Salesperson.objects.get(id=sp_id)
        content["salesperson"] = salesperson
        cust_id = content["customer"]
        customer = Customer.objects.get(id=cust_id)
        content["customer"] = customer
        auto_id = content["vin"]
        auto = AutomobileVO.objects.get(id=auto_id)
        content["vin"] = auto
        salesrec = Sales.objects.create(**content)
        return JsonResponse(
            salesrec,
            encoder=SalesEncoder,
            safe=False,
        )


@require_http_methods(['DELETE', 'GET', 'PUT'])
def salesrec_detail(request, id):
    if request.method == 'GET':
        sale = get_object_or_404(Sales, id=id)
        return JsonResponse(
            sale,
            encoder=SalesEncoder,
            safe=False,
        )
    elif request.method == 'DELETE':
        count, _ = Sales.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Sales.objects.filter(id=id).update(**content)
        sale = get_object_or_404(Sales, id=id)
        return JsonResponse(
            sale,
            encoder=SalesEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def vins_list(request):
    if request.method == "GET":
        vins = AutomobileVO.objects.all()
        vin_list = []
        for vin in vins:
            vin_list.append({
                "id": vin.id,
                "vin": vin.import_vin,
            })
        return JsonResponse(
            {"vins_list": vin_list},
            encoder=AutomobileVOEncoder,
            safe=False,
        )
