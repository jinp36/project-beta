from common.json import ModelEncoder
from .models import Salesperson, Customer, Sales, AutomobileVO


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_vin", "import_href"]


class SalespersonListEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "employee_no",
        "first_name",
        "last_name",
    ]


class SalespersonDetailEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "employee_no",
        "first_name",
        "last_name",
    ]


class CustomerListEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "phone_no",
        "address"
    ]


class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "first_name",
        "last_name",
        "phone_no",
        "address"
    ]
    


class SalesEncoder(ModelEncoder):
    model = Sales
    properties = [
        "id",
        "vin",
        "salesperson",
        "customer",
        "price",
    ]
    encoders = {
        "vin": AutomobileVOEncoder(),
        "salesperson": SalespersonDetailEncoder(),
        "customer": CustomerDetailEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "emp_no": o.salesperson.employee_no,
            "sr_name": f"{o.salesperson.first_name} {o.salesperson.last_name}",
            "cust_name": f"{o.customer.first_name} {o.customer.last_name}",
            "import_vin": o.vin.import_vin,
            }
