import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something
from sales_rest.models import AutomobileVO

def get_vin():
    try:
        response = requests.get("http://carcar-inventory-api-1:8000/api/automobiles")
        content = json.loads(response.content)
        for auto in content["autos"]:
            AutomobileVO.objects.update_or_create(
                import_href=auto["href"],
                defaults={"import_vin": auto["vin"]}
            )

    except requests.ConnectionError as e:
        print(e, file=sys.stderr)

def poll():
    while True:
        print('Sales poller polling for data')
        try:
            get_vin()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(5)


if __name__ == "__main__":
    poll()
