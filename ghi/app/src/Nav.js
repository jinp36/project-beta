import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="manufacturerDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Manufacturers
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="manufacturerDropdown">
                <li><NavLink className="dropdown-item" to="/manufacturer">View Manufacturers</NavLink></li>
                <li><NavLink className="dropdown-item" to="/manufacturer/new">Add a Manufacturer</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="modelDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Models
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="modelDropdown">
                <li><NavLink className="dropdown-item" to="/model">View Models</NavLink></li>
                <li><NavLink className="dropdown-item" to="/model/new">Add a Model</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="automobileDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Automobiles
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="automobileDropdown">
                <li><NavLink className="dropdown-item" to="/automobiles">View Automobiles</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobiles/new">Add an Automobile</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="salespeopleDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Salespeople
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="salespeopleDropdown">
                <li><NavLink className="dropdown-item" to="/salespeople">View Salespeople</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespeople/new">Add a Salesperson</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="customersDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Customers
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="customersDropdown">
                <li><NavLink className="dropdown-item" to="/customers">View Customers</NavLink></li>
                <li><NavLink className="dropdown-item" to="/customers/new">Add a Customer</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="techniciansDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Technicians
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="techniciansDropdown">
                <li><NavLink className="dropdown-item" to="/technicians">View Technicians</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians/new">Add a Technician</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="salesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="salesDropdown">
                <li><NavLink className="dropdown-item" to="/sales">View Sales</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales/new">Add a Sale</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales/history">Sales History</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" id="serviceDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Service
              </NavLink>
              <ul className="dropdown-menu" aria-labelledby="serviceDropdown">
                <li><NavLink className="dropdown-item" to="/appointments">View Appointments</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/new">Add an Appointment</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/history">Service History</NavLink></li>
              </ul>
            </li>


          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
