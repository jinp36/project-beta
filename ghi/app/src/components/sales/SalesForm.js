import React, { useState, useEffect } from 'react';

export default function SalesForm() {
    const [vins, setVins] = useState([]);
    const [salespersons, setSalespersons] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [formData, setFormData] = useState({
        vin: '',
        salesperson: '',
        customer: '',
        price: ''
    });

    const getVins = async () => {
        const vinUrl = 'http://localhost:8090/api/vins/'
        const response = await fetch(vinUrl);
        if (response.ok) {
            const data = await response.json();
            setVins(data.vins_list);
            }
        };

    useEffect(() => {
        getVins();
    }, []);

    const getSalesperson = async () => {
        const spUrl = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(spUrl);

        if (response.ok) {
            const data = await response.json();
            const salespersons = data.sales_persons.map(sp => {
                return {
                    "id": sp.id,
                    "employee_no": sp.employee_no,
                    "first_name":sp.first_name,
                    "last_name":sp.last_name,
                }
            });
            setSalespersons(salespersons);
        };
    };


    useEffect(() => {
        getSalesperson();
    }, []);

    const getCustomer = async () => {
        const custUrl = 'http://localhost:8090/api/customers/'
        const response = await fetch(custUrl);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
            };
    };

    useEffect(() => {
        getCustomer();
    }, []);

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                vin: '',
                salesperson: '',
                customer: '',
                price: ''
            });
        }
    };

    return (
        <div className='row'>
            <div className='offset-3 col-6 mb-5 mt-5'>
                <div className='shadow p-4 mt-3'>
                    <h1 className='text-center'>Add a New Sale</h1>
                    <form onSubmit={handleSubmit} id='add-sale-form'>
                        <div className='form-floating mb-3'>
                            <select value={formData.vin} onChange={handleFormChange} placeholder='Select Vehicle' required
                                name='vin' className='form-select' >
                                    <option value="">Select Vehicle VIN</option>
                                    {vins.map(vin => {
                                        return (
                                            <option key={vin.id} value={vin.id}>
                                                {vin.vin}
                                            </option>
                                        );
                                    })}
                            </select>
                        </div>
                        <div className='form-floating mb-3'>
                            <select value={formData.salesperson} onChange={handleFormChange} placeholder='Select Salesperson' required
                                name='salesperson' className='form-select' >
                                    <option value="">Select Salesperson</option>
                                    {salespersons.map(sp => {
                                        return (
                                            <option key={sp.id} value={sp.id}>
                                                {`${sp.first_name} ${sp.last_name}`}
                                            </option>
                                        );
                                    })}
                            </select>
                        </div>
                        <div className='form-floating mb-3'>
                            <select value={formData.customer} onChange={handleFormChange} placeholder='Select Customer' required
                                name='customer' className='form-select' >
                                    <option value="">Select Customer</option>
                                    {customers.map(cust => {
                                        return (
                                            <option key={cust.id} value={cust.id}>
                                                {`${cust.first_name} ${cust.last_name}`}
                                            </option>
                                        );
                                    })}
                            </select>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.price} placeholder='price' required type="number"
                            name='price' id='price' className='form-control' />
                            <label htmlFor="price">Price</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    );
};
