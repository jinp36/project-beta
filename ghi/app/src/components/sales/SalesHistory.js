import React, { useState, useEffect } from 'react';

export default function SalesList() {
    const [sales, setSales] = useState([]);
    const [sp, setSP] = useState([]);
    const [selectsp, setSelectsp] = useState('');

    const getSales = async () => {
        const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            const spdata = data.sales_records.map((rec) => {
                return {
                    id: rec.id,
                    price: rec.price,
                    vin: rec.import_vin,
                    cust_name: rec.cust_name,
                    sr_name: rec.sr_name
                }
            })

            setSales(spdata);
            const sp = [];
            for (const sd of spdata) {
                if (!sp.includes(sd.sr_name)) {
                    sp.push(sd.sr_name)
                }
            }
            setSP(sp);
    }};

    useEffect(() => {
        getSales();
    }, []);

    const handleSelectChange = (e) => {
        setSelectsp(e.target.value);
    }

    const deleteSales = async (id) => {
        const deleteUrl = `http://localhost:8090/api/sales/${id}/`;

            const response = await fetch(deleteUrl, { method: 'DELETE' });

            if (response.ok) {
                setSales(sales.filter(s => s.id !== id));
        }
    };

    return (
        <div id="form-row" className='row'>
            <div className='container-responsive shadow p-4'>
                <h1 className='text-center'>Sales by Salesperson</h1>
                <div className='mb-3'>
                    <select value={selectsp} onChange={handleSelectChange} placeholder='Select Salesperson' required
                        name='salesperson' className='form-select' >
                            <option value="">Select Salesperson</option>
                            {sp.map((salesp, index) => {
                                return (
                                    <option key={index} value={salesp}>
                                        {salesp}
                                    </option>
                                );
                            })}
                    </select>
                </div>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>Salesperson Name</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                    {sales
                        .filter(sale => sale.sr_name === selectsp)
                        .map((sale) => (
                        <tr key={sale.id}>
                            <td>{sale.sr_name}</td>
                            <td>{sale.cust_name}</td>
                            <td>{sale.vin}</td>
                            <td>{sale.price}</td>
                            <td>
                                <button className='btn btn-danger' onClick={() => deleteSales(sale.id)}>Delete</button>
                            </td>
                        </tr>
                    ))}
                </tbody>
                </table>
            </div>
        </div>
    )
};
