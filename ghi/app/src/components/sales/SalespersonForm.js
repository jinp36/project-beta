import React, { useState } from 'react';

export default function SalespersonForm() {
    const [formData, setFormData] = useState({
        employee_no: '',
        first_name: '',
        last_name: ''
    });

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const salespersonUrl = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                employee_no: '',
                first_name: '',
                last_name: ''

            });
        }
    };

    return (
        <div className='row'>
            <div className='offset-3 col-6 mb-5 mt-5'>
                <div className='shadow p-4 mt-3'>
                    <h1 className='text-center'>Add a Salesperson</h1>
                    <form onSubmit={handleSubmit} id='add-hats'>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.employee_no} placeholder='employee_no' required type="text"
                            name='employee_no' id='employee_no' className='form-control' />
                            <label htmlFor="employee_no">Employee ID</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.first_name} placeholder='first_name' required type="text"
                            name='first_name' id='first_name' className='form-control' />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.last_name} placeholder='last_name' required type="text"
                            name='last_name' id='last_name' className='form-control' />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    );
};
