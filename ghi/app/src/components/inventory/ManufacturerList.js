import React, { useState, useEffect } from 'react';

export default function ManufacturerList() {
    const [manufacturers, setManufacturer] = useState([]);

    const getManufacturers = async () => {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const data = await response.json();
            setManufacturer(data.manufacturers);
        }
    };

    useEffect(() => {
        getManufacturers();
    }, []);

    const deleteManufacturer = async (id) => {
        const deleteUrl = `http://localhost:8100/api/manufacturers/${id}/`;
            const response = await fetch(deleteUrl, { method: 'DELETE' });
            if (response.ok) {
                setManufacturer(manufacturers.filter(m => m.id !== id));
        }
    };

    return (
        <div id="form-row" className='row'>
            <div className='container-responsive shadow p-4'>
                <h1 className='text-center'>Manufacturers</h1>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Manufacturer</th>
                        </tr>
                    </thead>
                    <tbody>
                        {manufacturers.map((manufacturer) => (
                        <tr key={manufacturer.id}>
                            <td>{manufacturer.id}</td>
                            <td>{manufacturer.name}</td>
                            <td>
                                <button className='btn btn-danger' onClick={() => deleteManufacturer(manufacturer.id)}>Delete</button>
                            </td>
                        </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
};
