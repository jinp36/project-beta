import React, { useState, useEffect } from 'react'

export default function AutomobileForm() {
    const [models, setModels] = useState([]);

    const [formData, setFormData] = useState({
        color: '',
        year: '',
        model_id: '',
        vin: '',
    })

    const handleSubmit = async (event) => {
        event.preventDefault();
        const autoUrl = 'http://localhost:8100/api/automobiles/'

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            },
        };
        const response = await fetch(autoUrl, fetchConfig);
        if (response.ok) {
            setFormData({
                color: '',
                year: '',
                model_id: '',
                vin: '',
            });
        }
    }
    const handleFormChange = (event) => {
        const value = event.target.value;
        const inputName = event.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    const fetchModel = async () => {
    const modelUrl = 'http://localhost:8100/api/models/'
        const response = await fetch(modelUrl);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }
    useEffect(() => {
        fetchModel();
    }, []);

    return (
            <div className='row'>
                <div className='offset-3 col-6 mb-5 mt-5'>
                    <div className='shadow p-4 mt-3'>
                        <h1 className='text-center'>Add an Automobile</h1>
                        <form onSubmit={handleSubmit} id='create-automobile-form'>
                            <div className='form-floating mb-3'>
                                <input onChange={handleFormChange} value={formData.color} placeholder='Color' required type="text" name='color' id='color' className='form-control' />
                                <label htmlFor="name">Color</label>
                            </div>
                            <div className='form-floating mb-3'>
                                <input onChange={handleFormChange} value={formData.year} placeholder='Year' required type="text" name='year' id="year" className='form-control'/>
                                <label htmlFor='year'>Year</label>
                            </div>
                            <div className='form-floating mb-3'>
                                <input onChange={handleFormChange} value={formData.vin} placeholder='Vin Number' required type="text" name='vin' id='vin' className='form-control' />
                                <label htmlFor="vin">Vin Number</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleFormChange} value={formData.model_id} required name="model_id" id="model_id" className="form-select">
                                    <option>Choose a Model</option>
                                    {models.map(model => {
                                    return (
                                        <option key={model.id} value={model.id}> {model.manufacturer.name} {model.name} </option>
                                    )
                                    })}
                                </select>
                            </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    );
};
