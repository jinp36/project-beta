import React, { useState } from 'react';

export default function ManufacturerForm() {
    const [formData, setFormData] = useState({
        name: ''
    });

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        const blankUrl = 'http://localhost:8100/api/manufacturers/';

        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const response = await fetch(blankUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                name: ''
            });
        }
    };

    return (
        <div className='row'>
            <div className='offset-3 col-6 mb-5 mt-5'>
                <div className='shadow p-4 mt-3'>
                    <h1 className='text-center'>Add a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id='add-hats'>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.name} placeholder='name' required type="text"
                            name='name' id='name' className='form-control' />
                            <label htmlFor="name">Name</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    );
};
