import React, { useState, useEffect } from 'react';

export default function ModelForm() {

    const [selectManufacturer, setSelectManufacturer] = useState('');
    const [formData, setFormData] = useState({
        name: '',
        picture_url: ''
    });

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
            ...formData,
            [inputName]: value
        });
    };

    const [manufacturers, setManufacturers] = useState([]);

    const handleManufChange = (e) => {
        const value = e.target.value;
        return setSelectManufacturer(value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.name = formData.name;
        data.picture_url = formData.picture_url;
        data.manufacturer_id = selectManufacturer;
        const modelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const response = await fetch(modelUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
                picture_url: ''
            });
            setSelectManufacturer('');
        }
    };

    const fetchManuf = async () => {
        const manufUrl = 'http://localhost:8100/api/manufacturers/'

        const response = await fetch(manufUrl);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(() => {
        fetchManuf();
    }, []);

    return (
        <div className='row'>
            <div className='offset-3 col-6 mb-5 mt-5'>
                <div className='shadow p-4 mt-3'>
                    <h1 className='text-center'>Add a Model</h1>
                    <form onSubmit={handleSubmit} id='add-hats'>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.name} placeholder='name' required type="text"
                            name='name' id='name' className='form-control' />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange} value={formData.picture_url} placeholder='picture_url' required type="text"
                            name='picture_url' id='picture_url' className='form-control' />
                            <label htmlFor="picture_url">Picture Url</label>
                        </div>
                        <div className='mb-3'>
                            <select value={selectManufacturer} onChange={handleManufChange} placeholder='Select Manufacturer' required
                            name='manufacturer' className='form-select' >
                                <option value="">Select Manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    );
};
