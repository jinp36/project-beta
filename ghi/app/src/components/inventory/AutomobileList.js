import React, { useState, useEffect } from 'react'

export default function AutomobileList() {
    const [autos, setAutos] = useState([]);

    const getAutomobiles = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    };

    useEffect(() => {
        getAutomobiles();
    }, []);

    const deleteAutomobile = async (id) => {
        const deleteUrl = `http://localhost:8100/api/automobiles/${id}/`;
            const response = await fetch(deleteUrl, { method: 'DELETE' });
            if (response.ok) {
                setAutos(autos.filter(a => a.id !== id));
        }
    };
    
    return(
            <div id="form-row" className='row'>
                <div className='container-responsive shadow p-4'>
                    <h1 className='text-center'>Automobiles</h1>
                    <table className='table table-striped'>
                        <thead>
                            <tr>
                                <th>Vin</th>
                                <th>Color</th>
                                <th>Year</th>
                                <th>Model</th>
                                <th>Manufacturer</th>
                                <th>Sold</th>
                            </tr>
                        </thead>
                        <tbody>
                            {autos.map((auto) => (
                            <tr key={auto.id}>
                                <td>{auto.vin}</td>
                                <td>{auto.color}</td>
                                <td>{auto.year}</td>
                                <td>{auto.model.name}</td>
                                <td>{auto.model.manufacturer.name}</td>
                                <td>{auto.sold ? "Yes" : "No"}</td>
                                <td>
                                <button className='btn btn-danger' onClick={() => deleteAutomobile(auto.id)}>Delete</button>
                                </td>
                            </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        )
    };
