import React, { useState, useEffect } from 'react';

export default function ModelList() {
    const [models, setModels] = useState([]);

    const getModel = async () => {
        const response = await fetch('http://localhost:8100/api/models/');
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    };

    useEffect(() => {
        getModel();
    }, []);

    const deleteModel = async (id) => {
        const deleteUrl = `http://localhost:8100/api/models/${id}/`;
            const response = await fetch(deleteUrl, { method: 'DELETE' });
            if (response.ok) {
                setModels(models.filter(m => m.id !== id));
        }
    };

    return (
        <div id="form-row" className='row'>
            <div className='container-responsive shadow p-4'>
                <h1 className='text-center'>Models</h1>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Picture</th>
                            <th>Manufacturer</th>
                        </tr>
                    </thead>
                    <tbody>
                        {models.map((model) => (
                        <tr key={model.id}>
                            <td>{model.name}</td>
                            <td className='w-25'><img src={model.picture_url} className='img-fluid img-thumbnail'></img></td>
                            <td>{model.manufacturer.name}</td>
                            <td>
                                <button className='btn btn-danger' onClick={() => deleteModel(model.id)}>Delete</button>
                            </td>
                        </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
};
