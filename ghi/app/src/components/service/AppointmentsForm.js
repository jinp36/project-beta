import React, { useState, useEffect } from 'react'

function AppointmentsForm() {

    const [technicians, setTechnicians] = useState([]);


    const fetchTechnician = async () => {
    const technicianUrl = 'http://localhost:8080/api/technicians/';
        const response = await fetch(technicianUrl);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    }
    useEffect(() => {
        fetchTechnician();
    }, []);

    const [formData, setFormData] = useState({
        vin: '',
        customer: '',
        date: '',
        technician:'',
        reason:'',
    })

    const handleSubmit = async (event) => {
        event.preventDefault();

        const appointmentUrl = 'http://localhost:8080/api/appointments/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json"
            },
        };
        const response = await fetch(appointmentUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                vin: '',
                customer: '',
                date: '',
                technician:'',
                reason:'',
            });
        }
        }
        const handleFormChange = (event) => {
            const value = event.target.value;
            const inputName = event.target.name;
            setFormData({
                ...formData,
                [inputName]: value
            });
        }
        return (
            <div className='row'>
                <div className='offset-3 col-6 mb-5 mt-5'>
                    <div className='shadow p-4 mt-3'>
                        <h1 className='text-center'>Add an appointment</h1>
                        <form onSubmit={handleSubmit} id='create-Appointment-form'>
                            <div className='form-floating mb-3'>
                                <input onChange={handleFormChange} value={formData.vin} placeholder='Vin' required type='text' name='vin' id='vin' className='form-control' />
                                <label htmlFor="vin">VIN</label>
                            </div>
                            <div className='form-floating mb-3'>
                                <input onChange={handleFormChange} value={formData.customer} placeholder='Customer' required type='text' name='customer' id='customer' className='form-control'/>
                                <label htmlFor='customer'>First and Last Name</label>
                            </div>
                            <div className='form-floating mb-3'>
                                <input onChange={handleFormChange} value={formData.date} placeholder='Date and Time' required type='datetime-local' name='date' id='date' className='form-control' />
                                <label htmlFor="date">Date and Time</label>
                            </div>
                            <div className='form-floating mb-3'>
                                <input onChange={handleFormChange} value={formData.reason} placeholder='Reason for appointment' required type='text' name='reason' id='reason' className='form-control' />
                                <label htmlFor="reason">Reason</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleFormChange} value={formData.technician.id} required name="technician" id="technician_id" className="form-select">
                                    <option>Choose a Technician</option>
                                    {technicians.map(technician => {
                                    return (
                                        <option key={technician.id} value={technician.id}> {technician.first_name} {technician.last_name} </option>
                                    )
                                    })}
                                </select>
                            </div>
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default AppointmentsForm;
