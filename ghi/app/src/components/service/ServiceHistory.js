import React, { useState, useEffect } from 'react'

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);

    const getAppointments = async () => {
        const response = await fetch('http://localhost:8080/api/appointments/');
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
            setSearchInput('');
        }
    };

    useEffect(() => {
        getAppointments();
    }, []);

    const [searchInput, setSearchInput] = useState('');

    const handleSearchChange = (event) => {
        event.preventDefault();
        setSearchInput(event.target.value);
    };
    const handleSearchSubmit = async (event) => {
        event.preventDefault();
        if(searchInput.length > 0) {
            const searchData = appointments.filter(appointment => appointment.vin===(searchInput));
            setAppointments(searchData);
            setSearchInput('');
            };

        }

    const deleteAppointment = async (id) => {
        const deleteAppointmentUrl =`http://localhost:8080/api/appointments/${id}`;

        const fetchConfig = {
            method: "DELETE",
            headers: {
                'Content-Type' : 'application/json'
            },
        }
        const response = await fetch (deleteAppointmentUrl,fetchConfig)

        if (response.ok){
            setAppointments(appointments.filter(appointment => appointment.id !== id))
        }
    }
    return(
        <div id="form-row" className='row'>
            <div className='container-responsive shadow p-4'>
                <h1 className='text-center'>Service History</h1>
                <form onSubmit={handleSearchSubmit}>
                <div className="input-group">
                    <div className="form-outline">
                        <input value={searchInput} placeholder='Search by VIN...' type="search" id="search"
                        className="form-control" onChange={handleSearchChange}/>
                    </div>
                    <button type="submit" className="btn btn-primary shadow-none">
                        <i className="fas fa-search">Search</i>
                    </button>
                    <button type="button" className="btn btn-secondary shadow-none" onClick={()=>getAppointments()}>Return</button>
                </div>
                </form>
                <table className='table table-striped'>
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>VIP</th>
                            <th>Customer</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.map((appointment) => (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.sold ? "Yes" : "No"}</td>
                            <td>{appointment.customer}</td>
                            <td>{new Date(appointment.date).toLocaleDateString()}</td>
                            <td>{new Date(appointment.date).toLocaleTimeString()}</td>
                            <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>
                            <td><button type="submit" className="btn btn-danger btn-sm" onClick={()=>deleteAppointment(appointment.id)}>Delete</button></td>
                        </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    )
    };
export default ServiceHistory;
