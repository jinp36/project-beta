import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './components/inventory/ManufacturerList'
import ManufacturerForm from './components/inventory/ManufacturerForm';
import ModelList from './components/inventory/ModelList';
import ModelForm from './components/inventory/ModelForm';
import SalespersonList from './components/sales/SalespersonList';
import SalespersonForm from './components/sales/SalespersonForm';
import CustomerList from './components/sales/CustomerList';
import CustomerForm from './components/sales/CustomerForm';
import SalesList from './components/sales/SalesList';
import SalesForm from './components/sales/SalesForm';
import AutomobileList from './components/inventory/AutomobileList';
import AutomobileForm from './components/inventory/AutomobileForm';
import TechnicianList from './components/service/TechnicianList';
import TechnicianForm from './components/service/TechnicianForm';
import SalesHistory from './components/sales/SalesHistory';
import AppointmentsList from './components/service/AppointmentsList';
import AppointmentsForm from './components/service/AppointmentsForm';
import ServiceHistory from './components/service/ServiceHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
      <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturer">
            <Route index element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm  />} />
          </Route>
          <Route path="/model">
            <Route index element={<ModelList />} />
            <Route path="new" element={<ModelForm  />} />
          </Route>
          <Route path="/salespeople">
            <Route index element={<SalespersonList />} />
            <Route path="new" element={<SalespersonForm  />} />
          </Route>
          <Route path="/customers">
            <Route index element={<CustomerList />} />
            <Route path="new" element={<CustomerForm  />} />
          </Route>
          <Route path="/customers">
            <Route index element={<CustomerList />} />
            <Route path="new" element={<CustomerForm  />} />
          </Route>
          <Route path="/sales">
            <Route index element={<SalesList />} />
            <Route path="new" element={<SalesForm  />} />
            <Route path="history" element={<SalesHistory  />} />
          </Route>
          <Route path="/automobiles">
            <Route index element={<AutomobileList />} />
            <Route path="new" element={<AutomobileForm  />} />
          </Route>
          <Route path="/technicians">
            <Route index element={<TechnicianList />} />
            <Route path="new" element={<TechnicianForm  />} />
          </Route>
          <Route path="/appointments">
            <Route index element={<AppointmentsList />} />
            <Route path="new" element={<AppointmentsForm  />} />
            <Route path="history" element={<ServiceHistory  />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
